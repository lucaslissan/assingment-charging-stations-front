const express = require('express');

const app = express();
app.use(express.static("app"));
app.get('/', (req, res) => {
    res.redirect('/');
});

// Start the server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`);
});