'use strict';

angular.module('myApp')

.controller('View1Ctrl', function($scope, $http) {

  $scope.getNearestChargingStations = function() {
    if($scope.validateLatitudeAndLongitude()) {
      $scope.showMap = false;
      $scope.loading = true;
      $http({
        method: 'GET',
        url: "https://assignmentchargingstations.uc.r.appspot.com/nearest-charging-stations?latitude="+ $scope.latitude +"&longitude=" + $scope.longitude,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhdXRoMCIsImlhdCI6MTYyNDE1ODU5NSwiZXhwIjoxNjU1NzgwOTk1LCJhdWQiOiJ3d3cuZXhhbXBsZS5jb20iLCJzdWIiOiJqcm9ja2V0QGV4YW1wbGUuY29tIiwiR2l2ZW5OYW1lIjoiSm9obm55IiwiU3VybmFtZSI6IlJvY2tldCIsIkVtYWlsIjoianJvY2tldEBleGFtcGxlLmNvbSIsIlJvbGUiOlsiTWFuYWdlciIsIlByb2plY3QgQWRtaW5pc3RyYXRvciJdfQ.gPt2bJzOtbUNVtNxZxwAsEM3md-PCbu9gXD2-iRgPqI'
        }
      }).then(function successCallback(response) {
        var data = response.data;
          $scope.printMap(data);
          $scope.showMap = true;
      }, function errorCallback(response) {
        var data = response.data;
        console.log(data);
      }).finally(function() {
          $scope.loading = false;
      });
    }
  };

    $scope.printMap = function(markers){

      $scope.MapOptions = {
          center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      $scope.InfoWindow = new google.maps.InfoWindow();
      $scope.Latlngbounds = new google.maps.LatLngBounds();
      $scope.Map = new google.maps.Map(document.getElementById("dvMap"), $scope.MapOptions);

      for (var i = 0; i < markers.length; i++) {
          var data = markers[i];
          var myLatlng = new google.maps.LatLng(data.latitude, data.longitude);

          var marker = new google.maps.Marker({
              position: myLatlng,
              map: $scope.Map,
              title: data.title
          });

          (function (marker, data) {
              google.maps.event.addListener(marker, "click", function (e) {
                  $scope.InfoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
                  $scope.InfoWindow.open($scope.Map, marker);
              });
          })(marker, data);

          $scope.Latlngbounds.extend(marker.position);
      }

      $scope.Map.setCenter($scope.Latlngbounds.getCenter());
      $scope.Map.fitBounds($scope.Latlngbounds);
  };

    $scope.validateLatitudeAndLongitude = function(){
        if($scope.latitude != null && $scope.longitude != null){
            if(($scope.latitude >= -90.0 && $scope.latitude <= 90.0) && (($scope.longitude >= -180.0 && $scope.longitude <= 180.0))){
                return true;
            }
            alert("The values for latitude or longitude are not valid or are out of range.");
            return false;
        }else{
            alert("Please insert a value for latitude and a value for longitude");
            return false;
        }
    };

});