'use strict';

var app = angular.module("myApp", ['ngRoute']);

app.config(function($routeProvider) {
    $routeProvider
        .when('/view1', {
            templateUrl: 'view1/view1.html',
            controller: 'View1Ctrl'
        })
        .otherwise({
            redirectTo: '/view1'
        });
});